package com.ruoyi.web.controller.common;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.web.service.IStatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: devjd
 * @Date: 2022/3/11
 * @Description:
 */
@RestController
@RequestMapping("/statistics")
public class StatisticsController extends BaseController {

    @Autowired
    private IStatisticsService statisticsService;

    @GetMapping("/info")
    public AjaxResult index(){
        Map<String,Object> result=new HashMap<>();
        Map<String, Object> tenantInfo = statisticsService.tenantInfo();
        result.put("tenant_info", tenantInfo);
        return  AjaxResult.success(result);
    }

}
