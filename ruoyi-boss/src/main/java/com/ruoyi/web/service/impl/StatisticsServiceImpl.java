package com.ruoyi.web.service.impl;

import com.ruoyi.tenant.domain.MasterTenant;
import com.ruoyi.tenant.mapper.MasterTenantMapper;
import com.ruoyi.web.service.IStatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: devjd
 * @Date: 2022/3/11
 * @Description:
 */
@Service
public class StatisticsServiceImpl implements IStatisticsService {

    @Autowired
    private MasterTenantMapper tenantMapper;

    @Override
    public Map<String, Object> tenantInfo() {
        Map<String,Object> map=new HashMap<>();
        List<MasterTenant> masterTenants = tenantMapper.selectTodayRegister();
        int count = tenantMapper.countTenant();
        map.put("total_today", masterTenants.size());
        map.put("total_tenant",count);
        return map;
    }
}
