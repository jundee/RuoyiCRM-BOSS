package com.ruoyi.web.service;

import java.util.Map;

/**
 * 统计服务
 * @Author: devjd
 * @Date: 2022/3/11
 * @Description:
 */
public interface IStatisticsService {
    Map<String, Object> tenantInfo();
}
