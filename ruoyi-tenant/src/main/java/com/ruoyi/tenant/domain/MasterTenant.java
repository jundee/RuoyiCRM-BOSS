package com.ruoyi.tenant.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;

/**
 * 租户管理对象 master_tenant
 * 
 * @author ruoyi
 * @date 2022-03-11
 */
@Data
public class MasterTenant extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 租户编号 */
    @Excel(name = "租户编号")
    private String tenant;

    /** 数据库连接URL */
    @Excel(name = "数据库连接URL")
    private String url;

    /** 用户名 */
    @Excel(name = "用户名")
    private String username;

    /** 密码 */
    @Excel(name = "密码")
    private String password;

    /** 数据库名 */
    @Excel(name = "数据库名")
    private String databaseName;

    /** 数据库主机 */
    @Excel(name = "数据库主机")
    private String hostName;

    /** 状态(1正常 2停止) */
    @Excel(name = "状态(1正常 2停止)")
    private String status;

    /** 到期日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "到期日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date expirationDate;
}
