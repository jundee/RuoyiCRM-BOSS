package com.ruoyi.tenant.mapper;

import java.util.List;
import com.ruoyi.tenant.domain.MasterTenant;

/**
 * 租户管理Mapper接口
 * 
 * @author ruoyi
 * @date 2022-03-11
 */
public interface MasterTenantMapper
{
    /**
     * 查询租户管理
     * 
     * @param id 租户管理主键
     * @return 租户管理
     */
     MasterTenant selectMasterTenantById(Long id);

    /**
     * 查询租户管理列表
     * 
     * @param masterTenant 租户管理
     * @return 租户管理集合
     */
     List<MasterTenant> selectMasterTenantList(MasterTenant masterTenant);

    /**
     * 新增租户管理
     * 
     * @param masterTenant 租户管理
     * @return 结果
     */
     int insertMasterTenant(MasterTenant masterTenant);

    /**
     * 修改租户管理
     * 
     * @param masterTenant 租户管理
     * @return 结果
     */
     int updateMasterTenant(MasterTenant masterTenant);

    /**
     * 删除租户管理
     * 
     * @param id 租户管理主键
     * @return 结果
     */
     int deleteMasterTenantById(Long id);

    /**
     * 批量删除租户管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
     int deleteMasterTenantByIds(Long[] ids);


    int countTenant();

    List<MasterTenant> selectTodayRegister();
}
