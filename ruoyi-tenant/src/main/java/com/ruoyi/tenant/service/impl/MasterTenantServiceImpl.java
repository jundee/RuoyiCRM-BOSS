package com.ruoyi.tenant.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.tenant.mapper.MasterTenantMapper;
import com.ruoyi.tenant.domain.MasterTenant;
import com.ruoyi.tenant.service.IMasterTenantService;

/**
 * 租户管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-03-11
 */
@Service
public class MasterTenantServiceImpl implements IMasterTenantService 
{
    @Autowired
    private MasterTenantMapper masterTenantMapper;

    /**
     * 查询租户管理
     * 
     * @param id 租户管理主键
     * @return 租户管理
     */
    @Override
    public MasterTenant selectMasterTenantById(Long id)
    {
        return masterTenantMapper.selectMasterTenantById(id);
    }

    /**
     * 查询租户管理列表
     * 
     * @param masterTenant 租户管理
     * @return 租户管理
     */
    @Override
    public List<MasterTenant> selectMasterTenantList(MasterTenant masterTenant)
    {
        return masterTenantMapper.selectMasterTenantList(masterTenant);
    }

    /**
     * 新增租户管理
     * 
     * @param masterTenant 租户管理
     * @return 结果
     */
    @Override
    public int insertMasterTenant(MasterTenant masterTenant)
    {
        masterTenant.setCreateTime(DateUtils.getNowDate());
        return masterTenantMapper.insertMasterTenant(masterTenant);
    }

    /**
     * 修改租户管理
     * 
     * @param masterTenant 租户管理
     * @return 结果
     */
    @Override
    public int updateMasterTenant(MasterTenant masterTenant)
    {
        return masterTenantMapper.updateMasterTenant(masterTenant);
    }

    /**
     * 批量删除租户管理
     * 
     * @param ids 需要删除的租户管理主键
     * @return 结果
     */
    @Override
    public int deleteMasterTenantByIds(Long[] ids)
    {
        return masterTenantMapper.deleteMasterTenantByIds(ids);
    }

    /**
     * 删除租户管理信息
     * 
     * @param id 租户管理主键
     * @return 结果
     */
    @Override
    public int deleteMasterTenantById(Long id)
    {
        return masterTenantMapper.deleteMasterTenantById(id);
    }

    @Override
    public int countTenant() {
        return masterTenantMapper.countTenant();
    }

    @Override
    public List<MasterTenant> selectTodayRegister() {
        return masterTenantMapper.selectTodayRegister();
    }
}
